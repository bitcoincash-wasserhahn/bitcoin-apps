/* Import modules. */
const debug = require('debug')('bitcoinfilesjs-network')

/* Set sleep delay. */
const sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms))

/**
 * Bitcoin Files Protocol (BFP) Network
 */
class BFPNetwork {
    /* Constructor. */
    constructor(_bitbox) {
        /* Initialize BITBOX. */
        this.bitbox = _bitbox

        /* Set stop pay monitor flag. */
        this.stopPayMonitor = false

        /* Set monitoring payment flag. */
        this.isMonitoringPayment = false
    }

    /**
     * Get Last UTXO with Retry
     */
    async getLastUtxoWithRetry(_address, _retries = 40) {
        let result
        let count = 0

        /* Make requests. */
        while(typeof result === 'undefined') {
            /* Retrieve last UTXO. */
            result = await this.getLastUtxo(_address)
                .catch(err => console.error(err))
            console.log(result)

            /* Increment counter. */
            count++

            /* Validate retries. */
            if (count > _retries) {
                throw new Error('BITBOX.Address.utxo endpoint experienced a problem')
            }

            /* Sleep. */
            await sleep(250)
        }

        /* Return result. */
        return result
    }

    /**
     * Get Transaction Details with Retry
     */
    async getTransactionDetailsWithRetry(_txid, _retries = 40){
        let result
        let count = 0

        /* Make requests. */
        while(result === undefined) {
            /* Request transaction details. */
            result = await this.bitbox.Transaction.details(_txid)
                .catch(err => console.error(err))

            /* Increment counter. */
            count++

            /* Validate retries. */
            if (count > _retries) {
                throw new Error('BITBOX.Address.details endpoint experienced a problem')
            }

            /* Sleep. */
            await sleep(250)
        }

        return result
    }

    /**
     * Get Last UTXO
     */
    async getLastUtxo(_address) {
        /* Set cash address flag. */
        const isCashAddress = this.bitbox.Address
            .isCashAddress(_address)

        /* Set legacy address flag. */
        const isLegacyAddress = this.bitbox.Address
            .isLegacyAddress(_address)

        /* Validate address. */
        // NOTE: Must be a cash or legacy address.
        if (!isCashAddress && !isLegacyAddress) {
            throw new Error('Not an a valid address format, must be cashAddr or Legacy address format.')
        }

        /* Request (last) UTXO. */
        const res = (
            await this.bitbox.Address.utxo([ _address ])
                .catch(err => console.error(err))
        )[0]

        /* Handle response. */
        if (res && res.utxos && res.utxos.length > 0) {
            /* Return (last) UTXO. */
            return res.utxos[0]
        }

        /* Return response. */
        return res
    }

    /**
     * Send Transaction
     */
    async sendTx(_hex) {
        /* Send raw transaction. */
        const res = await this.bitbox.RawTransactions
            .sendRawTransaction(_hex)
            .catch(err => console.error(err))

        /* Handle response. */
        if (res && res.error) {
            return undefined
        }

        /* Handle response. */
        if (res === '64: too-long-mempool-chain') {
            throw new Error('Mempool chain too long')
        }

        /* Handle log. */
        debug(`sendTx() res: ${res}`)

        /* Return response. */
        return res
    }

    /**
     * Send Transaction with Retry
     */
    async sendTxWithRetry(_hex, _retries = 10) {
        let res
        let count = 0

        /* Make requests. */
        while (res === undefined || res.length!==64) {
            res = await this.sendTx(_hex)
                .catch(err => console.error(err))

            /* Increment count. */
            count++

            /* Validate retries. */
            if (count > _retries) {
                break
            }

            /* Sleep. */
            await sleep(250)
        }

        /* Validate response length. */
        // NOTE: Expecting a transaction id.
        if (res.length !== 64) {
            throw new Error('BITBOX network error')
        }

        /* Return response. */
        return res
    }

    /**
     * Monitor for Payment
     */
    async monitorForPayment(_paymentAddress, _fee, _onPaymentCb) {
        if (this.isMonitoringPayment || this.stopPayMonitor) {
            /* Return. */
            return
        }

        /* Set monitoring payment flag. */
        this.isMonitoringPayment = true

        /* Set cash address. */
        const isCashAddress = this.bitbox.Address
            .isCashAddress(_paymentAddress)

        /* Set legacy address. */
        const isLegacyAddress = this.bitbox.Address
            .isLegacyAddress(_paymentAddress)

        /* Validate addresses. */
        // NOTE: Must be a cash or legacy address.
        if (!isCashAddress && !isLegacyAddress) {
            throw new Error('Not an a valid address format, must be cashAddr or Legacy address format.')
        }

        /* Initialize UTXO. */
        let utxo = null

        /* Start monitoring. */
        while (true) { // eslint-disable-line no-constant-condition
            try {
                /* Request UTXO details. */
                utxo = await this.getLastUtxo(_paymentAddress)
                    .catch(err => console.error(err))

                /* Validate UTXO. */
                if (utxo && utxo.satoshis >= _fee && utxo.confirmations === 0) {
                    break
                }
            } catch (err) {
                console.log('monitorForPayment() error: ', err)
            }

            /* Validate payment monitor. */
            if (this.stopPayMonitor) {
                /* Set monitoring payment flag. */
                this.isMonitoringPayment = false

                /* Return. */
                return
            }

            /* Sleep. */
            await sleep(2000)
        }

        /* Set monitoring payment flag. */
        this.isMonitoringPayment = false

        /* Handle payment callback. */
        _onPaymentCb(utxo)
    }
}

/* Export module.*/
module.exports = BFPNetwork
