/* Import module. */
const BfpUtils = require('./utils')
const BITBOX = require('bitbox-sdk').BITBOX
const bitbox = new BITBOX()

/**
 * Build Metadata Transaction
 */
const buildMetadataTx = function (_config) {
    console.log('\nbuildMetadataTx (config):)', _config)

    /* Initialize transaction builder. */
    let transactionBuilder = null

    /* Handle network. */
    transactionBuilder = new bitbox.TransactionBuilder('bitcoincash')

    /* Initialize satoshis. */
    let inputSatoshis = 0

    /* Add input. */
    transactionBuilder.addInput(
        _config.input_utxo.txid,
        _config.input_utxo.vout
    )

    /* Add satoshis. */
    inputSatoshis += _config.input_utxo.satoshis

    /* Calculate metadata fee. */
    // TODO: Create method for calculating miner fee.
    let metadataFee = BfpUtils.calculateMetadataMinerFee(
        _config.bfpMetadataOpReturn.length)

    /* Calculate output. */
    let output = inputSatoshis - metadataFee

    /* Add output. */
    // NOTE: Metadata OP_RETURN
    transactionBuilder.addOutput(_config.bfpMetadataOpReturn, 0)

    /* Set output address. */
    let outputAddress = bitbox.Address
        .toCashAddress(_config.fileReceiverAddress)

    /* Add output. */
    transactionBuilder.addOutput(outputAddress, output)

    /* Set payment keypair. */
    // NOTE: sign inputs
    const paymentKeyPair = bitbox.ECPair
        .fromWIF(_config.input_utxo.wif)

    /* Sign transaction. */
    transactionBuilder.sign(
        0,
        paymentKeyPair,
        null,
        transactionBuilder.hashTypes.SIGHASH_ALL,
        _config.input_utxo.satoshis
    )

    /* Return "built" transaction. */
    return transactionBuilder.build()
}

/* Export module. */
module.exports = buildMetadataTx
